import React, { useEffect, useState, useCallback } from 'react';
import fire from '../../utils/fire.js'

export const AuthContext = React.createContext();

export const AuthProvider = ({ children}) => {
    const handleSignUp = useCallback(async event => {
        event.preventDefault();
        const { email, password, passwordConfrmation } = event.target.elements;        
        if (password !== passwordConfrmation) {
                throw alert("Passwords not match")
            }
        else {
            try {
                await fire.auth().createUserWithEmailAndPassword(email.value, password.value);
            }  catch (error) {
                alert(error);
            }
        }

    }, [])
    
    const handleLogin = useCallback(async event => {
        event.preventDefault();
        const { email, password } = event.target.elements;
        try {
            await fire.auth().signInWithEmailAndPassword(email.value, password.value);
        } catch (error) {
            alert(error);
        }
    }, [])

    const [currentUser, setCurrentUser] = useState([]
        // localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')): []
    )
    
    useEffect(() => {
        fire.auth().onAuthStateChanged( user => {
            setCurrentUser(user);
            // localStorage.setItem('userData', JSON.stringify(user));
        })
    }, [])

    return (
        <AuthContext.Provider value= {{currentUser, handleLogin, handleSignUp}}>
            {children}
        </AuthContext.Provider>
    )
}
