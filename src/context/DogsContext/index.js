import React, {useEffect, useState} from 'react';

export const DogsContext = React.createContext();

export const DogsProvider = ({children}) => {
    useEffect(() => {
        fetchDogs();
    }, [])
    const [loading, setLoading] = useState(true)
    const [dogs, setDogs] = useState([]);

    const fetchDogs = async () => {
        const data = await(fetch('https://dog.ceo/api/breeds/image/random/30'));
        const dogs = await data.json();
        setDogs(dogs.message);
        setLoading(false);
    }
    
    return(
        <DogsContext.Provider value={{dogs, loading}}>
            {children}
        </DogsContext.Provider>
    )
}
