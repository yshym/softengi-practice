import React, {useContext} from 'react';
import {Link, Redirect} from 'react-router-dom';
import { Button, Input, Form } from '../../components';
import { AuthContext } from '../../context';

export const Signup = () => {
    const {handleSignUp, currentUser} = useContext(AuthContext)
    if (currentUser) {
        return <Redirect to="/main" />;
    }

    return (
        <Form text="Sign Up" action={handleSignUp}>
            <Input type="text" name="email" text="Email" />
            <Input type="password" name="password" text="Password" />
            <Input type="password" name="passwordConfirmation" text="Password Confirmation" />
            <Button type="submit" text="Sign Up" />
            <span className="or-break">or</span>
            <Link to="/login" className="auth-link">Log In</Link>
        </Form>
    )
}
