import React from 'react';
import './App.css';
import {BrowserRouter, Switch, Route, Redirect, useRouteMatch} from 'react-router-dom';
import {PrivateRoute} from './components';
import Header from './components/Header/index'
import Sidebar from './components/Sidebar/index'
import {DogsTab, StarwarsTab, Home, Login, Signup} from './containers';
import {AuthProvider, DogsProvider, StarwarsProvider} from './context/';


const HomeScreen = ({url}) => {
  let match = useRouteMatch();
  return(
    <div className="mainframe">
      <Switch>
        <Route path={match.url} exact component={Home} />
        <DogsProvider>
          <StarwarsProvider>
            <Route path={`${match.url}/dogs`}  component={DogsTab} />
            <Route path={`${match.url}/starwars`}  component={StarwarsTab} />
          </StarwarsProvider>
        </DogsProvider>
      </Switch>
    </div>
  )
}

const App = ({history}) => {
  return (
    <AuthProvider>
      <BrowserRouter history={history}>
        <div className="wrap">
          <Header />
          <Sidebar />
          <div className="main-window" />
            <main> 
              <Switch>
                <Route path="/login" component={Login} />
                <Route path="/signup" component={Signup} />
                <PrivateRoute path="/main" component={HomeScreen} />
                <Redirect to="/login"/>
              </Switch>
            </main>
        </div>     
      </BrowserRouter>
    </AuthProvider>
    
  );
}

export default App;
